variable "namespace" {
  type = string
}

variable "chart_repository" {
  type    = string
  default = "{% if chart_repository %}{{ chart_repository }}{% elif gitlab_project_id %}https://gitlab.com/api/v4/projects/{{gitlab_project_id}}/packages/helm/stable{% endif %}"
}

variable "chart_name" {
  type = string
}

variable "chart_version" {
  type = string
}

variable "values" {
  type    = list(string)
  default = []
}

variable "helm_force_update" {
  type    = bool
  default = false
}

variable "helm_recreate_pods" {
  type    = bool
  default = false
}

variable "helm_cleanup_on_fail" {
  type    = bool
  default = false
}

variable "helm_atomic" {
  type    = bool
  default = true
}

variable "helm_timeout" {
  type    = number
  default = 300
}

variable "helm_max_history" {
  type    = number
  default = 10
}
