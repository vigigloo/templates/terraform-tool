{% set_global slug = slug | default(value=name | slugify)  -%}
resource "helm_release" "{{slug | addslashes}}" {
  chart           = "{{chart|default(value=slug)}}"
  repository      = var.chart_repository
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history
  atomic          = var.helm_atomic
  timeout         = var.helm_timeout

  values = var.values
}
